package com.luigivampa92.testtask.domain;

import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.luigivampa92.testtask.data.repository.ReviewRepository;
import com.luigivampa92.testtask.mvp.BasePresenter;
import com.luigivampa92.testtask.mvp.MainView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    @Inject ReviewRepository reviewRepository;

    private List<Review> reviews;
    private String lastQuery;

    public MainPresenter() {

        Log.d("TESTMOXY", "Presenter " + String.valueOf(this.hashCode()) +" constructor called");

        getAppComponent().inject(this);
        reviews = new ArrayList<>();
        lastQuery = "";
    }

    public void getReviews(@NonNull String query, boolean forceClear) {
        if (forceClear)
            reviews.clear();

        handleQuery(query);
        reviewRepository.getReviews(query, reviews.size())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        reviews -> {
                            this.reviews.addAll(reviews);
                            getViewState().displayReviews(this.reviews);
                            getViewState().completeLoading();
                },
                        e -> {
                            getViewState().toastMessage(e.getMessage());
                            getViewState().completeLoading();
                }
                );
    }

    private void handleQuery(String query) {
        if (!query.equals(lastQuery))
            reviews.clear();
        lastQuery = query;
    }
}
