package com.luigivampa92.testtask.domain;

public class Review {

    public String displayTitle;
    public String byline;
    public String headline;
    public String summaryShort;
    public String publicationDate;
    public String openingDate;
    public String dateUpdated;

    public String linkType;
    public String linkUrl;
    public String linkSuggestedText;

    public String mediaType;
    public String mediaSrc;
    public Integer mediaWidth;
    public Integer mediaHeight;

    public int criticsPick;
    public String rating;

    public Review() {}

    public Review(String displayTitle, String byline, String headline, String summaryShort,
                  String publicationDate, String openingDate, String dateUpdated, String linkType,
                  String linkUrl, String linkSuggestedText, String mediaType, String mediaSrc,
                  Integer mediaWidth, Integer mediaHeight, int criticsPick, String rating) {
        this.displayTitle = displayTitle;
        this.byline = byline;
        this.headline = headline;
        this.summaryShort = summaryShort;
        this.publicationDate = publicationDate;
        this.openingDate = openingDate;
        this.dateUpdated = dateUpdated;
        this.linkType = linkType;
        this.linkUrl = linkUrl;
        this.linkSuggestedText = linkSuggestedText;
        this.mediaType = mediaType;
        this.mediaSrc = mediaSrc;
        this.mediaWidth = mediaWidth;
        this.mediaHeight = mediaHeight;
        this.criticsPick = criticsPick;
        this.rating = rating;
    }
}
