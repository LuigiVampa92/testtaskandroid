package com.luigivampa92.testtask.mvp;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.luigivampa92.testtask.TrialApplication;
import com.luigivampa92.testtask.di.AppComponent;

public abstract class BasePresenter<V extends MvpView> extends MvpPresenter<V> {

    protected AppComponent getAppComponent() {
        return TrialApplication.getAppComponent();
    }
}
