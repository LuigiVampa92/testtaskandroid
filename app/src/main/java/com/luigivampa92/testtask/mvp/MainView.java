package com.luigivampa92.testtask.mvp;

import com.luigivampa92.testtask.domain.Review;

import java.util.List;

public interface MainView extends BaseView {

    void displayReviews(List<Review> reviews);

    void toastMessage(String message);

    void completeLoading();
}
