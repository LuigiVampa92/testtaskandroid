package com.luigivampa92.testtask.di;

import com.luigivampa92.testtask.domain.MainPresenter;
import com.luigivampa92.testtask.di.module.AppModule;
import com.luigivampa92.testtask.di.module.RestModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, RestModule.class})
public interface AppComponent {
    void inject(MainPresenter mainPresenter);
}
