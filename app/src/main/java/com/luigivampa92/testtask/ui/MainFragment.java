package com.luigivampa92.testtask.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.luigivampa92.testtask.domain.MainPresenter;
import com.luigivampa92.testtask.mvp.BaseFragment;
import com.luigivampa92.testtask.mvp.MainView;
import com.luigivampa92.testtask.R;
import com.luigivampa92.testtask.domain.Review;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends BaseFragment implements MainView {

    @InjectPresenter(type = PresenterType.GLOBAL, tag = "MainPresenter")
    MainPresenter presenter;

    @ProvidePresenter
    MainPresenter provideMainPresenter() {
        return new MainPresenter();
    }

    @BindView(R.id.recycler_view)         RecyclerView recyclerView;
    @BindView(R.id.progress_bar_loading)  ProgressBar progressBar;
    @BindView(R.id.progress_bar_fetching) ProgressBar progressBarFetching;
    @BindView(R.id.swipe_refresh_layout)  SwipeRefreshLayout swipeRefreshLayout;

    private MovieReviewRecyclerViewAdapter recyclerViewAdapter;
    private LinearLayoutManager recyclerViewLayoutManager;

    private boolean notOnceRequested = true;
    private boolean notOnceResponded = true;
    private boolean processingFetch = false;
    private int countPast, countVisible, countTotal;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TESTMOXY", "fragment " + String.valueOf(this.hashCode()) +" onCreate() called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TESTMOXY", "fragment " + String.valueOf(this.hashCode()) +" onDestroy() called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.d("TESTMOXY", "fragment " + String.valueOf(this.hashCode()) +" onCreateView() called");

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        if (notOnceResponded)
            new Handler().post(() -> progressBar.setVisibility(View.VISIBLE));

        Context context = view.getContext();
        recyclerViewAdapter = new MovieReviewRecyclerViewAdapter(this.getContext());
        recyclerViewLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);

        recyclerView.addOnScrollListener(OnScrollListener);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.getReviews(getCurrentQuery(), true));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (notOnceRequested) {
            notOnceRequested = false;
            presenter.getReviews(getCurrentQuery(), true);
        }
    }

    @Override
    public void displayReviews(List<Review> reviews) {
        recyclerViewAdapter.setItems(reviews);
    }

    @Override
    public void completeLoading() {
        notOnceResponded = false;
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        if (processingFetch && progressBarFetching.getVisibility() == View.VISIBLE) {
            processingFetch = false;
            progressBarFetching.setVisibility(View.GONE);
        }
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    public void searchReviewsWithQuery(String query) {
        presenter.getReviews(query, false);
    }

    private String getCurrentQuery() {
        return ((MainActivity) getActivity()).searchView.getQuery().toString();
    }

    private RecyclerView.OnScrollListener OnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0) {
                countVisible = recyclerViewLayoutManager.getChildCount();
                countTotal = recyclerViewLayoutManager.getItemCount();
                countPast = recyclerViewLayoutManager.findFirstVisibleItemPosition();
                if (!processingFetch) {
                    if ((countVisible + countPast) >= countTotal) {
                        processingFetch = true;
                        progressBarFetching.setVisibility(View.VISIBLE);
                        presenter.getReviews(getCurrentQuery(), false);
                    }
                }
            }
        }
    };
}
