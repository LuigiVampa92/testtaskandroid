package com.luigivampa92.testtask.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luigivampa92.testtask.R;
import com.luigivampa92.testtask.domain.Review;

import java.util.ArrayList;
import java.util.List;

public class MovieReviewRecyclerViewAdapter extends RecyclerView.Adapter<MovieReviewViewHolder> {

    private Context context;
    private List<Review> items;

    public MovieReviewRecyclerViewAdapter(Context context) {
        this.context = context;
        items = new ArrayList<>();
    }

    public void setItems(List<Review> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public MovieReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_movie_review, parent, false);
        return new MovieReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieReviewViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
