package com.luigivampa92.testtask.ui;

import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.luigivampa92.testtask.R;
import com.luigivampa92.testtask.mvp.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.search_view) SearchView searchView;

    private MainFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initToolbar();
        searchView.setOnQueryTextListener(this);

        Log.d("TESTMOXY", "activity " + String.valueOf(this.hashCode()) +" onCreate() called");

        if (savedInstanceState == null) {
            fragment = MainFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.layout_main_container, fragment)
                    .commit();
        }
        else {
            fragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.layout_main_container);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TESTMOXY", "activity " + String.valueOf(this.hashCode()) +" onDestroy() called");
    }

    private void initToolbar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        fragment.searchReviewsWithQuery(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
