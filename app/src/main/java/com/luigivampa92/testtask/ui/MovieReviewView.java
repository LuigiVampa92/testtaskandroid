package com.luigivampa92.testtask.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.luigivampa92.testtask.R;
import com.luigivampa92.testtask.domain.Review;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieReviewView extends RelativeLayout {

    @BindView(R.id.movie_review_image)        ImageView movieImage;
    @BindView(R.id.button_show_more)          Button buttonShowMore;
    @BindView(R.id.movie_review_title)        TextView movieTitle;
    @BindView(R.id.movie_review_date)         TextView reviewDate;
    @BindView(R.id.movie_review_text)         TextView reviewText;
    @BindView(R.id.movie_review_description)  TextView reviewDescription;
    @BindView(R.id.tags_container)            LinearLayout tagContainer;
    @BindView(R.id.movie_review_tag_critpick) TextView tagCriticsPick;
    @BindView(R.id.movie_review_tag_rating)   TextView tagRating;

    public MovieReviewView(Context context) {
        this(context, null);
    }

    public MovieReviewView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MovieReviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_movie_review_content, this, true);
        ButterKnife.bind(this);
    }

    public void setItem(Review review) {
        Picasso.with(getContext()).load(review.mediaSrc).into(movieImage);
        movieTitle.setText(review.displayTitle);
        reviewDate.setText(review.dateUpdated);
        reviewText.setText(review.headline);
        reviewDescription.setText(review.summaryShort);
        setTags(review);

        buttonShowMore.setOnClickListener(view -> {
            Uri uri = Uri.parse(review.linkUrl);
            Intent openBrowserIntent = new Intent(Intent.ACTION_VIEW, uri);
            getContext().startActivity(openBrowserIntent);
        });
    }

    private void setTags(Review review) {
        boolean showTagCriticsPick = review.criticsPick > 0;
        boolean showTagRating = review.rating != null && !review.rating.isEmpty();

        tagContainer.setVisibility(showTagCriticsPick || showTagRating ? VISIBLE : GONE);
        tagCriticsPick.setVisibility(showTagCriticsPick ? VISIBLE : GONE);
        if (showTagRating) {
            tagRating.setText(review.rating);
            tagRating.setVisibility(VISIBLE);
        }
        else {
            tagRating.setVisibility(GONE);
        }
    }
}
