package com.luigivampa92.testtask.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.luigivampa92.testtask.domain.Review;

public class MovieReviewViewHolder extends RecyclerView.ViewHolder {

    private MovieReviewView view;

    public MovieReviewViewHolder(View itemView) {
        super(itemView);
        view = (MovieReviewView) itemView;
    }

    public void bind(Review review) {
        view.setItem(review);
    }
}
