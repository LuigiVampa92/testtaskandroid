package com.luigivampa92.testtask.data.rest;

import android.content.res.Resources;

import com.luigivampa92.testtask.R;
import com.luigivampa92.testtask.data.rest.model.GetReviewResponse;
import com.luigivampa92.testtask.data.rest.model.ReviewRestModel;
import com.luigivampa92.testtask.domain.Review;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ReviewRestSource {

    private static final String PARAM_API_KEY = "api-key";
    private static final String PARAM_QUERY = "query";
    private static final String PARAM_OFFSET = "offset";

    private RestService restService;
    private Resources resources;

    @Inject
    ReviewRestSource(RestService restService, Resources resources) {
        this.restService = restService;
        this.resources = resources;
    }

    public Observable<List<Review>> getReviews(String query, int offset) {
        return restService.getReviews(getRequestParameters(query, offset)).map(this::map);
    }

    private Map<String, String> getRequestParameters(String query, int offset) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(PARAM_API_KEY, resources.getString(R.string.api_key));
        parameters.put(PARAM_OFFSET, String.valueOf(offset));
        if (query != null && !query.isEmpty())
            parameters.put(PARAM_QUERY, query);
        return parameters;
    }

    private List<Review> map(GetReviewResponse response) {
        List<Review> result = new ArrayList<>(response.numResults);
        for (ReviewRestModel model: response.results) {
            result.add(
                    new Review(
                            model.displayTitle,
                            model.byline,
                            model.headline,
                            model.summaryShort,
                            model.publicationDate,
                            model.openingDate,
                            model.dateUpdated,
                            model.link.type,
                            model.link.url,
                            model.link.suggestedLinkText,
                            model.multimedia != null ? model.multimedia.type : null,
                            model.multimedia != null ? model.multimedia.src : null,
                            model.multimedia != null ? model.multimedia.width : 0,
                            model.multimedia != null ? model.multimedia.height : 0,
                            model.cirticsPick != null ? model.cirticsPick : 0,
                            model.rating
                    )
            );
        }

        return result;
    }
}
