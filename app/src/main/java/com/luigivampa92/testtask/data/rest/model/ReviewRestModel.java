package com.luigivampa92.testtask.data.rest.model;

import com.google.gson.annotations.SerializedName;

public class ReviewRestModel {

    @SerializedName("display_title")
    public String displayTitle;

    @SerializedName("byline")
    public String byline;

    @SerializedName("headline")
    public String headline;

    @SerializedName("summary_short")
    public String summaryShort;

    @SerializedName("publication_date")
    public String publicationDate;

    @SerializedName("opening_date")
    public String openingDate;

    @SerializedName("date_updated")
    public String dateUpdated;

    @SerializedName("link")
    public ReviewLinkModel link;

    @SerializedName("multimedia")
    public ReviewMultimediaModel multimedia;

    @SerializedName("critics_pick")
    public Integer cirticsPick;

    @SerializedName("mpaa_rating")
    public String rating;

}
