package com.luigivampa92.testtask.data.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetReviewResponse {

    @SerializedName("status")
    public String status;

    @SerializedName("copyright")
    public String copyright;

    @SerializedName("has_more")
    public Boolean hasMore;

    @SerializedName("num_results")
    public Integer numResults;

    @SerializedName("results")
    public List<ReviewRestModel> results;

}
