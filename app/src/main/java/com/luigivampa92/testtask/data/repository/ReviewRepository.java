package com.luigivampa92.testtask.data.repository;

import com.luigivampa92.testtask.data.rest.ReviewRestSource;
import com.luigivampa92.testtask.domain.Review;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ReviewRepository {

    private ReviewRestSource restSource;

    @Inject
    public ReviewRepository(ReviewRestSource restSource) {
        this.restSource = restSource;
    }

    public Observable<List<Review>> getReviews(String query, int offset) {
        return restSource.getReviews(query, offset);
    }
}
