package com.luigivampa92.testtask.data.rest.model;

import com.google.gson.annotations.SerializedName;

public class ReviewMultimediaModel {

    @SerializedName("type")
    public String type;

    @SerializedName("src")
    public String src;

    @SerializedName("width")
    public Integer width;

    @SerializedName("height")
    public Integer height;

}
