package com.luigivampa92.testtask.data.rest.model;

import com.google.gson.annotations.SerializedName;

public class ReviewLinkModel {

    @SerializedName("type")
    public String type;

    @SerializedName("url")
    public String url;

    @SerializedName("suggested_link_text")
    public String suggestedLinkText;

}
