package com.luigivampa92.testtask.data.rest;


import com.luigivampa92.testtask.data.rest.model.GetReviewResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface RestService {

    @GET("reviews/search.json")
    Observable<GetReviewResponse> getReviews(@QueryMap Map<String, String> query);

}
