package com.luigivampa92.testtask;

import android.app.Application;

import com.luigivampa92.testtask.di.AppComponent;
import com.luigivampa92.testtask.di.DaggerAppComponent;
import com.luigivampa92.testtask.di.module.AppModule;

public class TrialApplication extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
